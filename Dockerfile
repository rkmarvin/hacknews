FROM python:3.8

RUN mkdir -p /app

WORKDIR /app

COPY ./req.txt /app/

RUN pip3 install -r req.txt

COPY ./ /app/
