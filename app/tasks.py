from . import parser
from .celery import app
from .db.repository import PostsRepository


@app.task
def update_posts():
    posts = parser.get_posts()
    posts_repository = PostsRepository()
    posts_repository.insert_if_not_exist_many(posts)
