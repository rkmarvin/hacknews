from celery import Celery


app = Celery()


class Config:
    enable_utc = True
    timezone = 'Europe/London'
    broker_url = 'redis://redis:6379/0'


app.config_from_object(Config)

app.conf.beat_schedule = {
    'planner': {'task': 'app.tasks.update_posts', 'schedule': 3 * 60.0}
}
