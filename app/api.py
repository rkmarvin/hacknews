import dataclasses
from typing import List

from db.posts import PostsItem
from db.repository import PostsRepository
from flask import Flask, request
from flask_restful import Api, Resource

app = Flask(__name__)
api = Api(app)


class Posts(Resource):
    def get(self) -> List[dict]:
        order = request.args.get('order', 'id')

        try:
            offset = int(request.args.get('offset'))
        except TypeError:
            offset = 0

        try:
            limit = int(request.args.get('limit'))
        except TypeError:
            limit = 5

        posts_repository = PostsRepository()
        posts = posts_repository.read_all(
            order=order, offset=offset, limit=limit
        )
        return self._to_dict(posts)

    def _to_dict(self, posts: List[PostsItem]) -> List[dict]:
        return list(map(lambda item: dataclasses.asdict(item), posts))


api.add_resource(Posts, '/posts')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5000')
