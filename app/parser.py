from datetime import datetime
from typing import List

import requests
from bs4 import BeautifulSoup

from .db.posts import PostsItem

NEWS_URL = 'https://news.ycombinator.com/'


def get_posts(url: str = NEWS_URL) -> List[PostsItem]:
    html = requests.get(url).content
    result = _parse(html)
    return result


def _parse(html: str) -> List[PostsItem]:
    soup = BeautifulSoup(html, features="html.parser")

    result = []
    posts = soup.find_all('tr', class_='athing')
    for item in posts:
        title = item.find('a', class_='storylink')

        if title is None:
            continue

        result.append(
            PostsItem(
                id=item.attrs['id'],
                title=title.text,
                url=title.attrs['href'],
                created=datetime.utcnow().isoformat(),
            )
        )

    return result
