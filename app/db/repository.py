import dataclasses
from typing import List

import pymongo
from pymongo import MongoClient

from .posts import PostsItem


class PostsRepository:
    def __init__(self):
        mongoclient = MongoClient('db', 27017)
        database = mongoclient.local
        self._posts = database.posts

    def insert_if_not_exist_many(self, posts: List[PostsItem]):
        if not posts:
            return

        posts_ids = [item.id for item in posts]
        exists_posts = [
            item['id']
            for item in self._posts.find({'id': {'$in': posts_ids}}, {'id': 1})
        ]
        self.insert_many(
            list(filter(lambda item: item.id not in exists_posts, posts))
        )

    def insert_many(self, posts: List[PostsItem]):
        if not posts:
            return

        self._posts.insert_many(self._prepare_data(posts))

    def read_all(self, order='id', offset=0, limit=5) -> List[PostsItem]:
        return list(
            map(
                lambda item: PostsItem(
                    id=item['id'],
                    title=item['title'],
                    url=item['url'],
                    created=item['created'],
                ),
                self._posts.find()
                .sort(self._sort_by(order))
                .skip(offset)
                .limit(limit),
            )
        )

    def _sort_by(self, order: str) -> dict:
        sign = (
            pymongo.DESCENDING if order.startswith('-') else pymongo.ASCENDING
        )
        field = order[1:] if order.startswith('-') else order
        return [(field, sign)]

    def _prepare_data(self, posts: List[PostsItem]) -> List[dict]:
        return list(map(lambda item: dataclasses.asdict(item), posts))
