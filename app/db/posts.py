from dataclasses import dataclass


@dataclass
class PostsItem:
    id: str
    title: str
    url: str
    created: str
